package ScaJass.Contracts

import ScaJass.Suit._
import ScaJass.{Card, Color, Contract}

object Trumpf extends BaseContract {
  override def getValue(card: Card, trumpf: Color.Value): Int =
    (card.suit, card.color) match {
      case (Jack, cardColor)
        if cardColor == trumpf => 20
      case (Nine, cardColor)
        if cardColor == trumpf => 14
      case (Eight, _) => 0
      case _ => super.getValue(card, trumpf)
    }

  override def getIndex(card: Card, trumpf: Color.Value): Int =
    (card.suit, card.color) match {
      case (Jack, cardColor) if cardColor == trumpf => -1
      case (Nine, cardColor) if cardColor == trumpf => 0
      case _ => super.getIndex(card, trumpf)
    }

  override def getWinningCard(cards: Seq[Card], trumpfColor: Color.Value): Card = {
    val orderedCards = cards.sortBy(c =>
      // Order by trumpf color, then played color and then points
      (c.color != trumpfColor, c.color != cards.head.color, getIndex(c, trumpfColor)))
    orderedCards.head
  }
}
