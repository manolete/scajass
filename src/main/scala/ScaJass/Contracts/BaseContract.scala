package ScaJass.Contracts

import ScaJass.Suit._
import ScaJass.{Card, Color, Contract, Suit}

object BaseContract {
  val cardSet: Set[Card] = {
    for {
      color <- Color.values
      suit <- Suit.values
    } yield Card(color, suit)
  }
}

abstract case class BaseContract(val numberOfPlayers: Int, val contract: Contract.Value) {
  val suitPointIndexMap = Map((Ace, (11, 1)), (King, (4, 2)), (Queen, (3, 3)), (Jack, (2, 4)), (Ten, (10, 5)), (Nine, (0, 6)), (Eight, (8, 7)), (Seven, (0, 8)), (Six, (0, 9)))

  def getValue(card: Card, color: Color.Value): Int = suitPointIndexMap.getOrElse(card.suit, throw new Exception("Unknown Card Suit"))._1

  def getIndex(card: Card, color: Color.Value): Int = suitPointIndexMap.getOrElse(card.suit, throw new Exception("Unknown Card Suit"))._2

  def getWinningCard(cards: Seq[Card], color: Color.Value): Card = {
    val orderedCards = cards.sortBy(c =>
      // Obenabe and first card decides upon color
      (c.color != cards.head.color, suitPointIndexMap.getOrElse(c.suit, throw new Exception("Unknown Card Suit"))._2))
    orderedCards.head
  }
}
