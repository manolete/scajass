package ScaJass

import ScaJass.Contracts.Trumpf
import scala.util.Random

case class Game(players: Seq[Player]) {
  private val fsm =
    FSM[Input, GameState] {
      case (GameTypeChoice(variant), ChooseGameType(`players`)) =>
        ChooseContract(players, variant)
      case (ContractChoice(contract), ChooseContract(`players`, variant)) if contract == Trumpf.contract =>
        ChooseColor(distributeCards(`players`), variant, contract)
      case (ContractChoice(contract), ChooseContract(activePlayer, variant)) if contract != Trumpf.contract =>
        // TODO: make color optional? or provide random color since it is not important?
        PlayerTurn(activePlayer, variant, contract, Color.Club, Seq.empty)
      case (ColorChoice(color), ChooseColor(`players`, variant, contract)) =>
        PlayerTurn(players, variant, contract, color, Seq.empty)
      case (CardPlayed(card), PlayerTurn(`players`, variant, contract, color, currentStack)) =>
        handleTurn(players, variant, contract, color, currentStack :+ card)
      case (_, state) => state
    }

  def put(input: Option[Input] = None, state: GameState = ChooseGameType(players)): GameState = {
    fsm.run(input map (i => List(i)) getOrElse List.empty).exec(state)
  }

  private def handleTurn(players: Seq[Player], variant: Variant.Value, contract: Contract.Value, color: Color.Value, currentStack: Seq[Card]): GameState = {
    // Nothing to do within a round
    val updatedPlayerHead = players.head.copy(cardsInHand = players.head.cardsInHand - currentStack.last)
    val updatedPlayers = players.drop(1) :+ updatedPlayerHead

    if (currentStack.size < players.size) {
      return PlayerTurn(updatedPlayers, variant, contract, color, currentStack)
    }

    val winningCard = contract match {
      case Contract.Trumpf => Trumpf.getWinningCard(currentStack, color)
      case _ => throw new NotImplementedError()
    }

    val wasLastCard = updatedPlayers.forall(p => p.cardsInHand.isEmpty)
    val winningPlayer = getWinningPlayer(winningCard, currentStack, updatedPlayers.last, updatedPlayers)
    val indexWinningPlayer = updatedPlayers.indexOf(winningPlayer)
    val updatedWinningPlayer = winningPlayer.copy(pile = winningPlayer.pile ++ currentStack, extraPoints = winningPlayer.extraPoints + (if (wasLastCard) 5 else 0))
    val updatedPlayersByWinner = (updatedWinningPlayer +: updatedPlayers.slice(indexWinningPlayer + 1, players.size)) ++ updatedPlayers.slice(0, indexWinningPlayer)
    if (wasLastCard) {
      ChooseColor(distributeCards(updatedPlayers), variant, contract)
    } else {
      PlayerTurn(updatedPlayersByWinner, variant, contract, color, Seq.empty)
    }
  }

  private def getWinningPlayer(winningCard: Card, currentStack: Seq[Card], player: Player, players: Seq[Player]) = {
    val indexOfCard = currentStack.indexOf(winningCard) + 1
    Iterator.continually(players)
      .flatten
      .dropWhile(p => p != player)
      .drop(players.size - indexOfCard)
      .next()
  }

  def distributeCards(players: Seq[Player]): Seq[Player] = {
    // TODO: derive the next line from the contract, not trumpf hardcoded...
    val randomizedCards: List[Set[Card]] = Random.shuffle(Trumpf.cardSet).grouped(9).toList
    players.zipWithIndex.map { case (p, i) => p.copy(cardsInHand = randomizedCards(i)) }
  }
}
