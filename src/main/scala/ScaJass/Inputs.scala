package ScaJass

sealed trait Input

case class GameTypeChoice(gameType: Variant.Value) extends Input

case class ContractChoice(contract: Contract.Value) extends Input

case class ColorChoice(color: Color.Value) extends Input

case class CardPlayed(card: Card) extends Input