package ScaJass

sealed trait GameState {
  // players, head is the player who's turn's next
  def players: Seq[Player]
}

case class ChooseGameType(players: Seq[Player]) extends GameState

case class ChooseContract(players: Seq[Player], variant: Variant.Value) extends GameState

case class ChooseColor(players: Seq[Player], variant: Variant.Value, contract: Contract.Value) extends GameState

case class PlayerTurn(players: Seq[Player],
                      variant: Variant.Value,
                      contract: Contract.Value,
                      color: Color.Value,
                      currentStack: Seq[Card]
                     ) extends GameState

case class Finished(players: Seq[Player]) extends GameState