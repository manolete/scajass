package ScaJass

object Color extends Enumeration {
  type Color = Value
  val Hearts, Spade, Diamond, Club = Value
}

object Suit extends Enumeration {
  type Type = Value
  val Ace, King, Queen, Jack, Ten, Nine, Eight, Seven, Six = Value
}

object Variant extends Enumeration {
  type Type = Value
  val Schieber, Coiffeur, Differenzler = Value
}

object Contract extends Enumeration {
  type Type = Value
  val Trumpf, Obenabe, Undenufe, Slalom = Value
}

// TODO: List combinations of variant and contract (e.g. Differenzler + Obenabe does not make sense)

case class Card(color: Color.Value, suit: Suit.Value)

case class Player(name: String, cardsInHand: Set[Card] = Set.empty, pile: Set[Card] = Set.empty, extraPoints: Int = 0)