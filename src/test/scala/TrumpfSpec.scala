import ScaJass.Contracts.Trumpf
import ScaJass.{Card, Color, Suit}

class TrumpfSpec extends ContractSpec(Trumpf) {
  val trumpfColor = Color.Club

  "Trumpf: Jack" should "beat Nine and Ace" in {
    val cards = Seq(Card(Color.Club, Suit.Nine), Card(Color.Club, Suit.Ace), Card(Color.Diamond, Suit.Nine), Card(Color.Club, Suit.Jack))
    val winningCard = Trumpf.getWinningCard(cards, trumpfColor)
    assert(winningCard == Card(Color.Club, Suit.Jack))
  }

  "Trumpf: Nine" should "beat Ace" in {
    val cards = Seq(Card(Color.Club, Suit.Nine), Card(Color.Club, Suit.Ace), Card(Color.Diamond, Suit.Nine), Card(Color.Diamond, Suit.Jack))
    val winningCard = Trumpf.getWinningCard(cards, trumpfColor)
    assert(winningCard == Card(Color.Club, Suit.Nine))
  }

  "Trumpf: Ace" should "beat King" in {
    val cards = Seq(Card(Color.Diamond, Suit.Nine), Card(Color.Club, Suit.Ace), Card(Color.Diamond, Suit.Nine), Card(Color.Club, Suit.King))
    val winningCard = Trumpf.getWinningCard(cards, trumpfColor)
    assert(winningCard == Card(Color.Club, Suit.Ace))
  }

  "Trumpf: Not Trumpf Queen" should "beat not Trumpf Jack and Ace of other Color" in {
    val cards = Seq(Card(Color.Diamond, Suit.Jack), Card(Color.Diamond, Suit.Queen), Card(Color.Spade, Suit.Ace), Card(Color.Diamond, Suit.Six))
    val winningCard = Trumpf.getWinningCard(cards, trumpfColor)
    assert(winningCard == Card(Color.Diamond, Suit.Queen))
  }
}