import ScaJass.Contracts.BaseContract
import ScaJass._

abstract class GameSpec(variant: Variant.Value, contract: BaseContract) extends ContractSpec(contract) {
  lazy val game = new Game(players)
  val initialState = game put()
  val chooseContractState = game put(Some(GameTypeChoice(variant)), initialState)

  "A newly started game" should "be in state ChooseGameType" in {

    val isInCorrectState: Boolean = initialState match {
      case ChooseGameType(_) => true
      case _ => false
    }
    assertResult(initialState.players.size)(4)
    assert(isInCorrectState)
  }
  private val players = for (i <- 1 to contract.numberOfPlayers) yield Player(name = i.toString)
  "Choosing a Variant" should "switch the state to ChooseContract" in {
    val isInCorrectState: Boolean = chooseContractState match {
      case ChooseContract(_, _) => true
      case _ => false
    }
    assertResult(chooseContractState.players.size)(4)
    assert(isInCorrectState)
  }
}
