import ScaJass.Contracts.Trumpf
import ScaJass.Variant.Schieber
import ScaJass._

class TrumpfGameSpec extends GameSpec(Schieber, Trumpf) {
  val colorChoiceState = game put(Some(ContractChoice(Contract.Trumpf)), chooseContractState)
  "Choosing a Contract" should "switch the state to ColorChoice" in {
    val isInCorrectState = colorChoiceState match {
      case ChooseColor(_, _, _) => true
      case _ => false
    }
    assertResult(colorChoiceState.players.size)(4)
    assert(isInCorrectState)
  }

  val playerTurnState = game put(Some(ColorChoice(Color.Club)), colorChoiceState)
  "Choosing a Color" should "switch the state to PlayerTurn and distribute cards to players" in {
    val isInCorrectState = playerTurnState match {
      case PlayerTurn(_, _, _, _, _) => true
      case _ => false
    }
    assert(isInCorrectState)
    assertResult(playerTurnState.players.size)(4)
    playerTurnState.players.foreach(p => assert(p.cardsInHand.size == 9))
  }
  val colorChoiceStateRoundFinished =
    (for (round <- 1 to 9; player <- 1 to 4) yield (player, round))
      .foldLeft(playerTurnState)(calculateNextState)

  def calculateNextState(currentState: GameState, playerRound: (Int, Int)): GameState = {
    val nextState = game put(Some(CardPlayed(currentState.players.head.cardsInHand.head)), currentState)
    val isLastCard = () => currentState.asInstanceOf[PlayerTurn].players.map(p => p.cardsInHand.size).sum == 1

    "Playing cards Player %s Round %s".format(playerRound._1, playerRound._2) should "keep the state in PlayerTurn or ChooseColor if last card played" in {
      val isInCorrectState = nextState match {
        case PlayerTurn(_, _, _, _, _) => true
        case ChooseColor(_, _, _) if isLastCard() => true
        case _ => false
      }
      assert(isInCorrectState)
    }

    if (nextState.players.groupBy(p => p.cardsInHand.size).size == 1 && !isLastCard()) {
      it should "every player should have %s cards".format(9 - playerRound._2) in {
        nextState.players.forall(p => p.cardsInHand.size == 9 - playerRound._2) // in round 1 after his turn a player should be left with 8 cards
      }
      it should "after each round the stack should be empty" in {
        assertResult(nextState.asInstanceOf[PlayerTurn].currentStack.size)(0)
      }
    } else {
      it should "put the player from the current state to the end of the list" in {
        assertResult(currentState.players.head.name)(nextState.players.last.name)
      }
    }

    it should "the number of players should be 4" in {
      assertResult(4)(nextState.players.size)
    }

    nextState
  }

  "After a full round the state" should "be in ChooseColor with new cards for every player" in {
    val isInCorrectState = colorChoiceStateRoundFinished match {
      case ChooseColor(players, _, _) if players.map(f => f.cardsInHand.size).sum == Trumpf.cardSet.size => true
      case _ => false
    }
    assert(isInCorrectState)

  }
}
