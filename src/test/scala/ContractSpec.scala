import ScaJass.Color
import ScaJass.Contracts.BaseContract
import org.scalatest.FlatSpec

abstract class ContractSpec(contract: BaseContract) extends FlatSpec {
  "A full card set" should "have 36 cards" in {
    assert(contract.cardSet.size == 36)
  }

  it should "have 152 points (missing 5 points from the last round)" in {
    val points = contract.cardSet.toSeq.map(i => contract.getValue(i, Color.Club))
    assert(points.sum == 152)
  }
}